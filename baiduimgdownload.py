# coding=utf-8
"""根据搜索词下载百度图片"""
"""python tkinter"""
__auther__ = {'name'    : 'wake_up_smiling',
              'Email'   : '1173341726@qq.com',
              'qq'      : '1173341726',
              'Created' :'2018-02-09'

}
import os
import re
import sys
import urllib.request
from tkinter import *
from tkinter import ttk
import requests
import threading
from urllib.request import urlretrieve
window=Tk()
window.title('百度图片批量下载')
window.geometry('328x260')
window.iconbitmap('./img/47.ico')
window.resizable(False, False)
var0 = StringVar()
var_path = StringVar()
var = IntVar()
var.set(1)
var1 = StringVar()
var2 = StringVar()

def start1():
	keyword = var0.get()
	os.mkdir(var_path.get())
	all_pic_urls = []
	for x in range(10):
		fanye_url = url_init_first + urllib.request.quote(keyword, safe='/') + '&pn=' + str(x*20)
		onepage_urls, fanye_url = get_onepage_urls(fanye_url)
		x += 1
		print('第%s页' % str(x))
		all_pic_urls.extend(onepage_urls)
	down_pic(list(set(all_pic_urls)))

def get_onepage_urls(onepageurl):
	try:
		html = requests.get(onepageurl).text
	except:
		print(e)
		pic_urls = []
		fanye_url = ''
		return pic_urls, fanye_url
	pic_urls = re.findall('"objURL":"(.*?)",', html, re.S)
	fanye_urls = re.findall(re.compile(r'<a href="(.*)" class="n">下一页</a>'), html, flags=0)
	fanye_url = 'http://image.baidu.com' + fanye_urls[0] if fanye_urls else ''
	return pic_urls, fanye_url


def down_pic(pic_urls):
	"""给出图片链接列表, 下载所有图片"""

	num = len(pic_urls)//4
	def fun01():
		x=0 
		for imgurl in pic_urls[:num]:
			try:
				print('正在下载第%s张图片' % str(x+1) )
				urlretrieve(imgurl,var_path.get()+'/%s.jpg' % str(x+1))
			except:
				print('下载第%s张图片时失败' % str(x+1))	
			x+=1

	def fun02():
		x = num
		for imgurl in pic_urls[num:num*2]:
			try:
				print('正在下载第%s张图片' % str(x+1) )
				urlretrieve(imgurl,var_path.get()+'/%s.jpg' % str(x+1))
			except:
				print('下载第%s张图片时失败' % str(x+1))	
			x+=1
			    
	def fun03():
		x=num*2
		for imgurl in pic_urls[num*2:num*3]:
			try:
				print('正在下载第%s张图片' % str(x+1))
				urlretrieve(imgurl,var_path.get()+'/%s.jpg' % str(x+1))
			except:
				print('下载第%s张图片时失败' % str(x+1))		
			x+=1

	def fun04():
		x = num*3
		for imgurl in pic_urls[x:]:
			try:
				print('正在下载第%s张图片' % str(x+1) )
				urlretrieve(imgurl,var_path.get()+'/%s.jpg' % str(x+1))
			except:
				print('下载第%s张图片时失败' % str(x+1))		
			x+=1
	t01 =threading.Thread(target=fun01)
	t02 =threading.Thread(target=fun02)
	t03 =threading.Thread(target=fun03)
	t04 =threading.Thread(target=fun04)
	t01.start()
	t02.start()
	t03.start()
	t04.start()
	t01.join()
	t02.join()
	t03.join()
	t04.join()
	print('下载完成 共%d张图片,已经保存在%s/下' % (len(os.listdir(var_path.get())),var_path.get()))
def screen():
	t2 = threading.Thread(target=screen1)
	t2.setDaemon(True)
	t2.start()
#执行筛选
def screen1():
    a = os.listdir(var_path.get())
    if var.get()==1:
        for item in a:
            if get_FileSize(var_path.get()+'/'+item) < int(var2.get()):
                os.remove(var_path.get()+'/'+item)
    else:
        for item in a:
            if get_FileSize(var_path.get()+'/'+item) > int(var2.get()):
                os.remove(var_path.get()+'/'+item)

def print_scale(v):
    var2.set(v)

#获取文件大小
def get_FileSize(filePath):
    fsize = os.path.getsize(filePath)
    fsize = fsize/float(1024)
    return round(fsize,2)


def start():
    t1 = threading.Thread(target=start1)
    t1.setDaemon(True)
    t1.start()
url_init_first = r'http://image.baidu.com/search/flip?tn=baiduimage&ie=utf-8&word='
# url_init_first = r'http://image.baidu.com/search/flip?tn=baiduimage&ipn=r&ct=201326592&cl=2&lm=-1&st=-1&fm=result&fr=&sf=1&fmq=1497491098685_R&pv=&ic=0&nc=1&z=&se=1&showtab=0&fb=0&width=&height=&face=0&istype=2&ie=utf-8&ctd=1497491098685%5E00_1519X735&word='
root = ttk.LabelFrame(window,text='search&download')
root.grid(column=0, row=0,padx=2, pady=2)
root1 = ttk.LabelFrame(window,text='screen')
root1.grid(column=0, row=1,padx=2, pady=2)
Label(root,text='图片检索:').grid(row=0,column=0)
Label(root,text='文件夹名称:').grid(row=1,column=0)
Entry(root,textvariable = var0).grid(row=0,column=1,padx=2, pady=2)
Entry(root,textvariable = var_path).grid(row=1,column=1,padx=2, pady=2)
b1=Button(root,text = '开始',command =start,height =2,width =6)
b1.grid(column=2, row=0,rowspan=2,padx=24, pady=2)

s1=Scale(root1,label='你滑一下试试！',from_=0,to=1000,orient=HORIZONTAL,
    length=265,showvalue=1,variable=var1,tickinterval=200,
    resolution=1,command=print_scale)
s1.grid(column=0, row=0,columnspan=3,padx=24, pady=2)


R1 = Radiobutton(root1, text="大于", variable=var, value=1)  
R1.grid( column=0,row=1)  
R2 = Radiobutton(root1, text="小于", variable=var, value=2,)  
R2.grid(column=1,row=1) 
Label(root1,text='KB',font=("仿宋", 17, "normal")).grid(row=2,column=1,pady=3)

e = Entry(root1,textvariable=var2,width=4,font=("仿宋", 17, "normal")).grid(row=2,column=0)
b2=Button(root1,text = '开始筛选',heigh=2,command =screen)
b2.grid(column=2, row=1,padx=2, rowspan=2,pady=2)
window.mainloop()
































